use "net/http"
use "net/ssl"
use "files"

class ResponseHandlerFactory is HandlerFactory
  let _main: Main

  new iso create(main: Main) =>
    _main = main

  fun apply(session: HTTPSession): HTTPHandler ref^ =>
    ResponseHandler.create(_main, session)

class ResponseHandler is HTTPHandler
  let _main: Main
  let _session: HTTPSession

  new ref create(main: Main, session: HTTPSession) =>
    _session = session
    _main = main

  fun ref apply(response: Payload val) =>
    _main.handle_response(response)


actor Main
  let _env: Env

  fun get_ssl_context() =>
    try
      let cert_file = FilePath(_env.root as AmbientAuth, "cacert.pem")
      SSLContext.>set_client_verify(true).>set_authority(cert_file)
    end

  new create(env: Env) =>
    _env = env

    let ssl_context = this.get_ssl_context()
    let url = "https://api.github.com/repos/ponylang/ponyc/issues"
    let method = "GET"

    try
      let client = HTTPClient(_env.root as AmbientAuth, ssl_context)
      let url' = URL.build(url)
      let handler = recover val ResponseHandlerFactory.create(this) end
      let request = Payload.request(method, url')
			request.update("User-Agent", "pony")
      client(consume request, handler)
    end

  be handle_response(response: Payload val) =>
    _env.out.print(response.status.string())
    try
      for chunk in response.body().values() do
        _env.out.write(chunk)
      end
    end
